//TODO: Add ability to choose department when registering an employee.-->
//TODO: Saving of employee in employees table and of department in departments table should be-->
//TODO: one  transaction (i.e., atomic)-->
//TODO: 2. Allow users to choose a department from a list of employees. Note: Function to call is in DeptService


// Always use an IIFE, i.e., (function() {})();
(function() {
    angular
        .module("EMS")          // to call an angular module, omit the second argument ([]) from the angular.module() syntax
        // this syntax is called the getter syntax
        .controller("RegCtrl", RegCtrl);    // angular.controller() attaches a controller to the angular module specified
                                            // as you can see, angular methods are chainable

    // TODO: 2.1 Inject DeptService so we can access appropriate function, Inject $filter because we need it to format dates
    RegCtrl.$inject = [ '$window', 'EmpService', 'DeptService' ];

    function RegCtrl( $window, EmpService, DeptService) {

        // Declares the var vm (for ViewModel) and assigns it the object this (in this case, the RegCtrl)
        // Any function or variable that you attach to vm will be exposed to callers of RegCtrl, e.g., index.html
        var vm = this;
        var today = new Date();
        var joindate = new Date();
        joindate.setFullYear(birthday.getFullYear() - 18);

        // Exposed data models ---------------------------------------------------------------------------------------
        // Creates an employee object that
        // We expose the employee object by attaching it to the vm
        // This will allow us apply two-way data-binding to this object by using ng-model in our view (i.e., index.html)
        vm.member = {
            memNo: "",
            firstname: "",
            lastname: "",
            gender: "M",
           joindate: today,
        //  hiredate: today,
            phonenumber: "",
            book: ""
        };

        // Creates a status object. We will use this to display appropriate success or error messages.
        vm.status = {
            message: "",
            code: ""
        };


        vm.register = register;

        // TODO: 2.2 Create function that would populate the department selection box with data. Logic should be placed
        // TODO: 2.2 in DeptService, but handling of success/error should be done in this controller

        initDepartmentBox();

        // Exposed functions -----------------------------------------------------------------------------------------

        function initDepartmentBox() {
            DeptService
                .retrieveBook()
                .then(function (results) {
                    console.log("--- Departments ----");
                    console.log(results.data);
                    vm.booked = results.data;
                })
                .catch(function (err) {
                    console.log("error " + JSON.stringify(err));
                    vm.status.code = err.data.parent.errno;
                });
        }

        // Function declaration and definition
        function register() {
            // Calls alert box and displays registration information
            alert("The registration information you sent are \n" + JSON.stringify(vm.member));

            // Prints registration information onto the client console
            console.log("The registration information you sent were:");
            console.log("Member Number: " + vm.member.memNo);
            console.log("Member First Name: " + vm.member.firstname);
            console.log("Member Last Name: " + vm.member.lastname);
            console.log("Member Gender: " + vm.member.gender);
           // console.log("Member Birthday: " + vm.member.birthday);
            console.log("Member Join Date: " + vm.member.joindate);
            console.log("MemberPhone Number: " + vm.member.phonenumber);
       //     console.log("Member Department name: " + vm.member.department);


            // We call EmpService.insertEmp to handle registration of employee information. The data sent to this
            // function will eventually be inserted into the database.
            EmpService
                .insertMem(vm.member)
                .then(function (result) {
                    console.log("result " + JSON.stringify(result));
                    $window.location.assign('/app/registration/thanks.html');
                })
                .catch(function (err) {
                    console.log("error " + JSON.stringify(err));
                    vm.status.message = err.data.name;
                    vm.status.code = err.data.parent.errno;
                });

        } // END function register()
    } // END RegCtrl
})();