// Defines client-side routing
(function () {
    angular
        .module("EMS")
        .config(UIRouterAppConfig)
        UIRouterAppConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function UIRouterAppConfig($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('edit', {
                url: '/edit',
                templateUrl: './app/edit/edit.html',
                controller: 'EditCtrl',
                controllerAs: 'ctrl'
            })
            .state('editWithParam', {
                url: '/edit/:empNo',
                templateUrl: './app/edit/edit.html',
                controller: 'EditCtrl',
                controllerAs: 'ctrl'
            })
            .state('register', {
                url: '/register',
                templateUrl: './app/registration/register.html',
                controller: 'RegCtrl',
                controllerAs: 'ctrl'
            })
            .state('search', {
                url: '/search',
                templateUrl: './app/search/search.html',
                controller: 'SearchCtrl',
                controllerAs: 'ctrl'
            })
            .state('searchDB', {
                url: '/searchDB',
                templateUrl: './app/search/searchDB.html',
                controller: 'SearchDBCtrl',
                controllerAs: 'ctrl'
            })
            .state('thanks', {
                url: '/thanks',
                templateUrl: './app/registration/thanks.html'
            })
            .state('index', {
                url: '/index',
                templateUrl: './index.html'

            })
               
            .state("signIn", {
                url: "/signIn",
           //     views: {
              //      "content": {
                        templateUrl: "./app/login/login.html",
                    
            //    }
                controller: 'LoginCtrl',
                controllerAs: 'ctrl'
            })

            .state('booking', {
                url: '/booking',
                templateUrl: './booking.html'

            })

            .state('add', {
                url: '/add',
                templateUrl: './app/add/addMember.html'

            })

            .state('delete', {
                url: '/delete',
                templateUrl: './app/delete/deleteMember.html'

            })

        $urlRouterProvider.otherwise("/register");
    }
})();