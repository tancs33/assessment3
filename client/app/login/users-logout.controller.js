/**
 * Created by phangty on 10/11/16.
 */
(function () {
  angular
        .module("EMS")
        .controller("LogoutCtrl", ["$state", LogoutCtrl]);

    function LogoutCtrl($state,){
        var vm = this;

        vm.logout = function () {
            logout()
                .then(function () {
                    $state.go("SignIn");
                }).catch(function () {
                console.error("Error logging on !");
            });
        };
    }
})();
