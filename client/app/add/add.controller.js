(function () {
    angular
        .module("EMS")
        .controller("MemberController", MemberController)
        .controller("EditMemberCtrl", EditMemberCtrl)
        .controller("AddMemberCtrl", AddMemberCtrl)
        .controller("DeleteMemberCtrl", DeleteMemberCtrl);
        
    MemberController.$inject = ['EMSAppAPI', '$uibModal', '$document', '$scope'];
    EditMemberCtrl.$inject = ['$uibModalInstance', 'EMSAppAPI', 'items', '$rootScope', '$scope'];
    AddMemberCtrl.$inject = ['$uibModalInstance', 'EMSAppAPI', 'items', '$rootScope', '$scope'];
    DeleteMemberCtrl.$inject = ['$uibModalInstance', 'EMSAppAPI', 'items', '$rootScope', '$scope'];
    
    function DeleteMemberCtrl($uibModalInstance, EMSAppAPI, items, $rootScope, $scope){
        var self = this;
        //self.items = items;
        self.deleteMember = deleteMember;
        console.log(items);
        EMSAppAPI.getMember(items).then((result)=>{
            console.log(result.data);
            self.member =  result.data;
            self.member.date_join = new Date( self.member.date_join);
    //      self.member.dateBooked = new Date( self.member.dateBooked);
            console.log(self.member.date_join);
        });

        function deleteMember(){
            console.log("delete member ...");
            EMSAppAPI.deleteMember(self.member.memID).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshMemberList');
                $uibModalInstance.close(self.run);
            }).catch((error)=>{
                console.log(error);
            });
        }

    }

    function AddMemberCtrl($uibModalInstance, EMSAppAPI, items, $rootScope, $scope){
        console.log("Add Member");
        var self = this;
        self.saveMember = saveMember;

     /*   self.member = {
            gender: "M"
        } */
        initializeCalendar($scope);
        function saveMember(){
            console.log("save member ...");
            console.log(self.member.first_name);
            console.log(self.member.last_name);
            console.log(self.member.date_join);
     //     console.log(self.member.dateBooked);
            console.log(self.member.gender);
            EMSAppAPI.addMember(self.member).then((result)=>{
                //console.log(result);
                console.log("Add member -> " + result.memID);
                $rootScope.$broadcast('refreshMemberListFromAdd', result.data);
             }).catch((error)=>{
                console.log(error);
                self.errorMessage = error;
             })
            $uibModalInstance.close(self.run);
        }
    }
        
    function initializeCalendar($scope){
        self.datePattern = /^\d{4}-\d{2}-\d{2}$/;;
        
        $scope.today = function() {
            $scope.dt = new Date();
        };
        $scope.today();
    
        $scope.clear = function() {
            $scope.dt = null;
        };
    
        $scope.inlineOptions = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
        };
    
        $scope.dateOptions = {
            //dateDisabled: disabled,
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1
        };

        // Disable weekend selection
        function disabled(data) {
            console.log(data);
            var date = data.date,
                mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }
    
        $scope.toggleMin = function() {
        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
        };
    
        $scope.toggleMin();
    
        $scope.open1 = function() {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function() {
            $scope.popup2.opened = true;
        };
    
        $scope.setDate = function(year, month, day) {
            $scope.dt = new Date(year, month, day);
        };
    
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy-MM-dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[1];
        $scope.altInputFormats = ['M!/d!/yyyy'];
    
        $scope.popup1 = {
            opened: false
        };

        $scope.popup2 = {
            opened: false
        };
    
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        $scope.events = [
        {
            date: tomorrow,
            status: 'full'
        },
        {
            date: afterTomorrow,
            status: 'partially'
        }
        ];
    
        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0,0,0,0);
        
                for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);
        
                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
                }
            }
        
            return '';
        }
    }

    function EditMemberCtrl($uibModalInstance, EMSAppAPI, items, $rootScope, $scope){
        console.log("Edit Member Ctrl");
        var self = this;
        self.items = items;
        initializeCalendar($scope);

        EMSAppAPI.getMember(items).then((result)=>{
           console.log(result.data);
           self.member =  result.data;
           self.member.date_join = new Date( self.member.date_join);
     //    self.member.dateBooked = new Date( self.member.dateBooked);
           console.log(self.member.date_join);
        })

        self.saveMember = saveMember;

        function saveMember(){
            console.log("save member ...");
            console.log(self.member.first_name);
            console.log(self.member.last_name);
            console.log(self.member.date_join);
  //        console.log(self.member.dateBooked);
            console.log(self.member.gender);
            EMSAppAPI.updateMember(self.member).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshMemberList');
             }).catch((error)=>{
                console.log(error);
             })
            $uibModalInstance.close(self.run);
        }

    }

    function MemberController(EMSAppAPI, $uibModal, $document, $scope) {
        var self = this;
        self.format = "M/d/yy h:mm:ss a";
        
        self.member = [];
        self.maxsize=5;
        self.totalItems = 0;
        self.itemsPerPage = 10;
        self.currentPage = 1;

        self.searchMember =  searchMember;
        self.addMember =  addMember;
        self.editMember = editMember;
        self.deleteMember = deleteMember;
        self.pageChanged = pageChanged;

        function searchAllMember(searchKeyword,orderby,itemsPerPage,currentPage){
            EMSAppAPI.searchMember(searchKeyword, orderby, itemsPerPage, currentPage).then((results)=>{
                self.member = results.data.rows;
                self.totalItems = results.data.count;
                //$scope.numPages = Math.ceil(self.totalItems /self.itemsPerPage);
            }).catch((error)=>{
                console.log(error);
            });
        }


        function pageChanged(){
            console.log("Page changed " + self.currentPage);
            searchAllMember(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
            console.log($scope.numPages);
        }

        $scope.$on("refreshMemberList",function(){
            console.log("refresh member list "+ self.searchKeyword);
            searchAllMember(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
        });

        $scope.$on("refreshMemberListFromAdd",function(event, args){
            console.log("refresh member list from memID"+ args.memID);
            var member = [];
            member.push(args);
            self.searchKeyword = "";
            self.member = member;
        });

        function searchMember(){
            console.log("search members  ....");
            console.log(self.orderby);
            searchAllMember(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
        }

        function addMember(size, parentSelector){
            console.log("post add member  ....");
            var items = [];
            var parentElem = parentSelector ? 
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
                
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/add/addMember.html',
                controller: 'AddMemberCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return items;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
            
        }

    /*    function editMember(memID, size, parentSelector){
            console.log("Edit Member...");
            console.log("memID > " + memID);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/editMember.html',
                controller: 'EditMemberCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return memID;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        } */

        function deleteMember(memID, size, parentSelector){
            console.log("delete Member...");
            console.log("memID > " + memID);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/delete/deleteMember.html',
                controller: 'DeleteMemberCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return memID;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }
    }
})();