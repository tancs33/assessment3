
// Always use an IIFE, i.e., (function() {})();
(function () {
    // Attaches EmpService service to the EMS module
    angular
        .module("EMS")
        .service("EmpService", EmpService);
        

    // Dependency injection. Here we inject $http because we need this built-in service to communicate with the server
    // There are different ways to inject dependencies; $inject is minification safe
    EmpService.$inject = ['$http'];

    // EmpService function declaration
    // Accepts the injected dependency as a parameter. We name it $http for consistency, but you may assign any name
    function EmpService($http) {

        // Declares the var service and assigns it the object this (in this case, the EmpService). Any function or
        // variable that you attach to service will be exposed to callers of EmpService, e.g., search.controller.js
        // and register.controller.js
        var service = this;

        // EXPOSED DATA MODELS -----------------------------------------------------------------------------------------
        // EXPOSED FUNCTIONS -------------------------------------------------------------------------------------------
        service.insertMem = insertMem;
        service.retrieveMem = retrieveMem;
        service.retrieveMemByNo = retrieveMemByNo;
        service.retrieveMemDB = retrieveMemDB;
        service.retrieveMemDept = retrieveMemDept;
        service.updateMemName = updateMemName ;
        service.deleteMem = deleteMem;

        // FUNCTION DECLARATION AND DEFINITION -------------------------------------------------------------------------
        // TODO: 4. Create function for deleting employee; search by  emp_no
        // deleteDept uses HTTP DELETE to delete department from database; passes information as route parameters.
        // IMPORTANT! Route parameters are not the same as query strings!
        function deleteMem(memID) {
            return $http({
                method: 'DELETE'
                , url: 'api/booking/' +  memID
            });

        }


        // insertEmp uses HTTP POST to send employee information to the server's /employees route
        // Parameters: employee information; Returns: Promise object
        function insertMem(member) {
            // This line returns the $http to the calling function
            // This configuration specifies that $http must send the employee data received from the calling function
            // to the /employees route using the HTTP POST method. $http returns a promise object. In this instance
            // the promise object is returned to the calling function
            return $http({
                method: 'POST'
                , url: 'api/booking'
                , data: {mem: member}
            });
        }

        // retrieveEmp retrieves employee information from the server via HTTP GET.
        // Parameters: none. Returns: Promise object
        function retrieveMem(){
            return $http({
                method: 'GET'
                , url: 'api/static/booking'
            });
        }

        // TODO: 4. Create function for retrieving specific emp; search by emp no
        function retrieveMemByNo(memID) {
            return $http({
                method: 'GET'
                , url: 'api/booking/' + memID
            });
        }


        // retrieveEmpDB retrieves employee information from the server via HTTP GET. Passes information via the query
        // string (params) Parameters: searchString. Returns: Promise object
        function retrieveMemDB(searchString){
            return $http({
                method: 'GET'
                , url: 'api/booking'
                , params: {
                    'searchString': searchString
                }
            });
        }

        // retrieveEmpDept retrieves employee and department information from the server via HTTP GET.
        // Parameters: searchString. Returns: Promise object
        function retrieveMemDept(searchString){
            return $http({
                method: 'GET'
                , url: 'api/booking/booked'
                , params: {
                    'searchString': searchString
                }
            });
        }

        // TODO: 4. Create function for updating emp first name of specific emp; search by emp no
        function updateMemName( memID, first_name) {
            return $http({
                method: 'PUT'
                , url: 'api/booking/' + memID
                , data: {
                    first_name: first_name
                }
            });
        }


    }
})();