(function(){
    angular
        .module("EMS")
        .service("EMSAppAPI", [
            '$http',
            EMSAppAPI
        ]);
    
    function EMSAppAPI($http){
        var self = this;

        // query string
        self.searchMember = function(value, sortby, itemsPerPage, currentPage){
            return $http.get(`/api/booking?keyword=${value}&sortby=${sortby}&itemsPerPage=${itemsPerPage}&currentPage=${currentPage}`);
        }

        self.getMember = function(memID){
            console.log(memID);
            return $http.get("/api/booking/" + memID)
        }

        self.updateMember= function(member){
            console.log(member);
            return $http.put("/api/booking",member);
        }

        self.deleteMember = function(memID){
            console.log(memID);
            return $http.delete("/api/booking/"+ memID);
        }
        
        // parameterized values
        /*
        self.searchEmployees = function(value){
            return $http.get("/api/employees/" + value);
        }*/

        // post by body over request
        self.addMember = function(member){
            return $http.post("/api/booking", member);
        }
    }
})();