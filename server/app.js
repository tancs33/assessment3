
// DEPENDENCIES ------------------------------------------------------------------------------------------------------
// Loads express module and assigns it to a var called express
var express = require("express");

// Loads path to access helper functions for working with files and directory paths
var path = require("path");

// Loads bodyParser to populate and parse the body property of the request object
var bodyParser = require("body-parser");
var session = require ("express-session");
var watch = require("connect-ensure-login");
var passport = require("passport");
// Loads sequelize ORM
var Sequelize = require("sequelize");

// CONSTANTS ---------------------------------------------------------------------------------------------------------
// Defines server port.
// Value of NODE_PORT is taken from the user environment if defined; port 3000 is used otherwise.
const NODE_PORT = process.env.NODE_PORT || 3000;

// Defines paths
// __dirname is a global that holds the directory name of the current module
const CLIENT_FOLDER = path.join(__dirname + '/../client');
const MSG_FOLDER = path.join(CLIENT_FOLDER + '/assets/messages');

// Defines MySQL configuration
const MYSQL_USERNAME = 'root';
const MYSQL_PASSWORD = 'vrujom';


// OTHER VARS ---------------------------------------------------------------------------------------------------------
//Create an instance of express application
var app = express();


// MIDDLEWARES --------------------------------------------------------------------------------------------------------
// Serves files from public directory (in this case CLIENT_FOLDER).
// __dirname is the absolute path of the application directory.
// if you have not defined a handler for "/" before this line, server will look for index.html in CLIENT_FOLDER
app.use(express.static(CLIENT_FOLDER));

// Populates req.body with information submitted through the registration form.
// Expected content type is application/x-www-form-urlencoded
//app.use(bodyParser.urlencoded({extended: false}));
// Default $http content type is application/json so we use json as the parser type
app.use(bodyParser.json());


// DBs, MODELS, and ASSOCIATIONS ---------------------------------------------------------------------------------------
// Creates a MySQL connection
var sequelize = new Sequelize(
    'member',
    MYSQL_USERNAME,
    MYSQL_PASSWORD,
    {
        host: 'localhost',         // default port    : 3306
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

var Member = require('./models/member')(sequelize, Sequelize);
var Item = require('./models/item')(sequelize, Sequelize);
var Book = require('./models/booked')(sequelize, Sequelize);
var BookDetail = require('./models/booking_detail')(sequelize, Sequelize);

//var Employee = require('./models/employees')(sequelize, Sequelize);

//var Department = require('./models/departments')(sequelize, Sequelize);
//var DeptEmp = require('./models/deptemp')(sequelize, Sequelize);

//Employee.hasMany(DeptEmp, {foreignKey: 'emp_no'})
//DeptEmp.hasOne(Department, {foreignKey: 'dept_no'});
//DeptEmp.belongsTo(Department, {foreignKey: 'dept_no'});


// ROUTE HANDLERS -----------------------------------------------------------------------------------------------------
// Defines endpoint exposed to client side for registration
app.post("/api/booking", function (req, res, next) {
    // Information sent via an HTTP POST is found in req.body
    console.log('\nInformation submitted to server:')
    console.log(req.body);
    
    var app = express();
    app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
    app.use(bodyParser.json({limit: '50mb'}));
     
    app.use(passport.initialize());
    app.use(passport.session());

    // Employee
    //     .create({
    //         emp_no: req.body.emp.empNo,
    //         birth_date: new Date(req.body.emp.birthday),
    //         first_name: req.body.emp.firstname,
    //         last_name: req.body.emp.lastname,
    //         gender: req.body.emp.gender,
    //         hire_date: new Date(req.body.emp.hiredate)
    //     })
    //     .then(function (employee) {
    //         res
    //             .status(200)
    //             .json(employee);
    //     })
    //     .catch(function (err) {
    //         console.log(err);
    //         res
    //             .status(501)
    //             .json(err);
    //     });

    sequelize
        .transaction(function (t) {
            return Member
                .create(
                    {
                        memID: req.body.mem.memNo,
                 //     birth_date: new Date(req.body.emp.birthday),
                        first_name: req.body.mem.firstname,
                        last_name: req.body.mem.lastname,
                        gender: req.body.mem.gender,
                        date_join: new Date(req.body.mem.joindate)
                    }
                    , {transaction: t})
                .then(function (member) {
                    console.log("inner result " + JSON.stringify(member))
                    return Book
                        .create(
                            {
                                dept_no: req.body.mem.bookingID
                                , dept_name: req.body.mem.dateBooked
                            }
                            , {transaction: t});
                });
        })
        .then(function (member) {
            res
                .status(200)
                .json(member);
        })
        .catch(function (err) {
            console.log(err);
            res
                .status(501)
                .json(err);
        });
});

// Defines endpoint handler exposed to client side for retrieving employee information from database. Client side
// sent data as part of the query string, we access query string paramters via the req.query property
app.get("/api/booking", function (req, res) {
    Member
    // findAll asks sequelize to retrieve multiple records (all records if where clause not used, i.e., no filtering)
        .findAll({
            where: {
                // This where condition filters the findAll result so that it only includes employee names and
                // employee numbers that have the searchstring as a substring (e.g., if user entered 's' as search
                // string, the following
                $or: [
                    {first_name: {$like: "%" + req.query.searchString + "%"}},
                    {last_name: {$like: "%" + req.query.searchString + "%"}},
                    {memID: {$like: "%" + req.query.searchString + "%"}}
                ]
            }
            // We add a limit since employees table is big
            , limit: 100
        })
        .then(function (member) {
            res
                .status(200)
                .json(member);
        })
        .catch(function (err) {
            res
                .status(500)
                .json(err);
        });
});


/* Defines endpoint handler exposed to client side for retrieving employee records that match query string passed.
 Match against dept name and dept no. Includes manager information. Client side sent data as part of the query
 string, we access query string paramters via the req.query property
 */
app.get("/api/booking/member", function (req, res) {
    Member
    // Use findAll to retrieve multiple records
        .findAll({
            // Use the where clause to filter final result; e.g., when you only want to retrieve employees that have
            // "s" in its name
            where: {
                // $or operator tells sequelize to retrieve record that match any of the condition
                $or: [
                    // $like + % tells sequelize that matching is not a strict matching, but a pattern match
                    // % allows you to match any string of zero or more characters
                    {first_name: {$like: "%" + req.query.searchString + "%"}},
                    {last_name: {$like: "%" + req.query.searchString + "%"}},
                    {memID: {$like: "%" + req.query.searchString + "%"}}
                ]
            }
            , limit: 100
            // What Include attribute does: Join two or more tables. In this instance:
            // 1. For every Employee record that matches the where condition, the include attribute returns
            // ALL employees that have served as managers of said Employee
            // 2. model attribute specifies which model to join with primary model
            // 3. order attribute specifies that the list of Managers be ordered from latest to earliest manager
            // 4. limit attribute specifies that only 1 record (in this case the latest manager) should be returned
            , include: [{
                model: BookingDetail
                , order: [["bookingDetailID", "DESC"]]
                , limit: 1
                // We include the Employee model to get the manager's name
                , include: [Book]
            }]
        })
        // this .then() handles successful findAll operation
        // in this example, findAll() used the callback function to return employees
        // we named it employees, but this object also contains info about the
        // latest department of that employee
        .then(function (member) {
            res
                .status(200)
                .json(member);
        })
        // this .catch() handles erroneous findAll operation
        .catch(function (err) {
            res
                .status(500)
                .json(err);
        });
});


// -- Searches for specific member by memID

app.get("/api/booking/:memID", function (req, res) {
    console.log
    var where = {};
    if (req.params.memID) {
        where.memID = req.params.memID
    }

    console.log("where " + where);
    // We use findOne because we know (by looking at the database schema) that emp_no is the primary key and
    // is therefore unique. We cannot use findByNo because findByNo does not support eager loading
    Member
        .findOne({
            where: where
            , include: [{
                model: member
                , order: [["frst_name", "DESC"]]
                , limit: 1
                // We include the Employee model to get the manager's name
           //     , include: [Book]
            }]
        })

        .then(function (member) {
            console.log("-- GET /api/booking/:memID findOne then() result \n " + JSON.stringify(member));
            res.json(member);
        })
        // this .catch() handles erroneous findAll operation
        .catch(function (err) {
            console.log("-- GET /api/booking/:memID findOne catch() \n ");

            res
                .status(500)
                .json({error: true});
        });
});

// -- Updates member
app.put('/api/booking/:memID', function (req, res) {
    var where = {};
    where.memID = req.params.memID;

    // Updates member detail
    console.log("body " + JSON.stringify(req.body));
    Member
        .update(
            {first_name: req.body.first_name}
            , {where: where}                            // search condition / criteria
        )
        .then(function (member) {
            res
                .status(200)
                .json(member);
        })
        .catch(function (err) {
            console.log(err);
            res
                .status(500)
                .json(err);
        });
});


// -- Searches for and deletes employee of a specific employee num
app.delete("/api/booking/:memID", function (req, res) {
    var where = {};
    where.memID = req.params.memID;

    Member
        .destroy({
            where: where
        })
        .then(function (result) {
            if (result == "1")
                res.json({success: true});
            else
                res.json({success: false});
        })
        .catch(function (err) {
            console.log("-- DELETE /api/booking/:memID catch(): \n" + JSON.stringify(err));
        });
});






// Defines endpoint exposed to client side for retrieving all employee information (STATIC)
app.get("/api/static/booking", function (req, res) {
    var member = [
        {
            memNo: 1001,
            memFirstName: 'Emily',
            memLastName: 'Smith',
            memPhoneNumber: '6516 2093'

        }
        , {
            memNo: 1002,
            memFirstName: 'Varsha',
            memLastName: 'Jansen',
            memPhoneNumber: '6516 2093'
        }
        ,
        {
            memNo: 1003,
            memFirstName: 'Julie',
            memLastName: 'Black',
            memPhoneNumber: '6516 2093'
        }
        , {
            memNo: 1004,
            memFirstName: 'Fara',
            memLastName: 'Johnson',
            memPhoneNumber: '6516 2093'
        }
        ,
        {
            memNo: 1005,
            memFirstName: 'Justin',
            memLastName: 'Zhang',
            memPhoneNumber: '6516 2093'
        }
        , {
            memNo: 1006,
            memFirstName: 'Kenneth',
            memLastName: 'Black',
            memPhoneNumber: '6516 2093'
        }

    ];
    // Return member as a json object
    res
        .status(200)
        .json(member);
});

app.get("/api/booked", function (req, res) {
    sequelize
    // Explanation of SQL statement
    // 1. SELECT - SELECT specifies that this is a read/retrieve command
    // 2. dept_no, ... - identifies the columns to return; use * to return all columns
    // 3. FROM departments - specifies the table to read data from
        .query("SELECT dateBooked, bookingID " +
            "FROM booked "
        )
        // this .spread() handles successful native query operation
        // we use .spread instead of .then so as to separate metadata from the emplooyee records
        .spread(function (booked) {
            res
                .status(200)
                .json(booked);
        })
        // this .catch() handles erroneous native query operation
        .catch(function (err) {
            res
                .status(500)
                .json(err);
        });
});




//delete one id
app.delete('/api/booking/:memID', (req, res)=>{
    console.log("one member ...");
    console.log(req.params.memID);
    var memID = req.params.memID;
    console.log(memID);
    var whereClause = {limit: 1, where: {memID: memID}};
    Member.findOne(whereClause).then((result)=>{
        result.destroy();
        res.status(200).json({});
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    }); 
});

app.use(function (req, res) {
    res.send("<h1>Page not found</h1>");
});
// Handles 404. In Express, 404 responses are not the result of an error,
// so the error-handler middleware will not capture them.
// To handle a 404 response, add a middleware function at the very bottom of the stack
// (below all other path handlers)
app.use(function (req, res) {
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

// Error handler: server error
app.use(function (err, req, res, next) {
    res.status(501).sendFile(path.join(MSG_FOLDER + '/501.html'));
});

// Server starts and listens on NODE_PORT
app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + NODE_PORT);
});