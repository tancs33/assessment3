// inform passport to use local strategy.
var LocalStrategy = require('passport-local').Strategy;
//var GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;
//var FacebookStrategy = require("passport-facebook").Strategy

var config = require('./config');

// this will depend on the localAuthenticate, this user defined callback function
function authenticateUser(username, password) {
    var userDB = config.USER_DATABASE;
    
    for (var i=0; i<userDB.length; i++) {
        var currentUser = userDB[i];
        if (username == currentUser.username) {
            return (password == currentUser.password);
        }
    }
    return false;
};

module.exports = function(app, passport) {
    //callback
    var localAuthenticate = function(username, password, done) {
        var valid = authenticateUser(username, password);
        if (valid) return done(null, username);
        return done(null, false);
    }

    // this is mandatory
    passport.use(new LocalStrategy(
        { // redefine the field names the stratgey (passport-local) expects
            usernameField: 'username',
            passwordField: 'password',
        },
        
        localAuthenticate // the strategy's "verify" callback
    ));

    function verifyCallback(accessToken, refreshToken, profile, done) {
        console.log("coming from social....");
      /*  if (profile.provider === 'google' || profile.provider === 'facebook') {
            id = profile.id;
            email = profile.emails[0].value;
            displayName = profile.displayName;
            provider_type = profile.provider;
            // insert to db using sequelize
            done(null, email);
        }else{ */
            done(null, false);
        }
 //   }

 /*   passport.use(new GoogleStrategy({
        clientID: config.GooglePlus_key,
        clientSecret: config.GooglePlus_secret,
        callbackURL: config.GooglePlus_callback_url
    }, verifyCallback))

    passport.use(new FacebookStrategy({
        clientID: config.Facebook_key,
        clientSecret: config.Facebook_secret,
        callbackURL: config.Facebook_callback_url,
        profileFields: ['id', 'displayName', 'photos', 'email']
    }, verifyCallback)) */

    passport.serializeUser(function(username, done) {
        //console.log('passport.serializeUser: ' + username);
        done(null, username);
      });
      
      passport.deserializeUser(function(id, done) {
        //console.log('passport.deserializeUser: ' + id);
        var user = id;
        done(null, user);
      });
      
      var isAuthenticated = function(req, res, next) {
        console.log("isAuthenticated(): ", req.user);
        if (req.user) {
          next();
        }else {
          res.sendStatus(401);
        }
      }
    
      return {
        isAuthenticated: isAuthenticated,
      }

};

