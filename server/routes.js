const HOME_PAGE = "/#!/index";
const SIGNIN_PAGE = "/#!/login";

module.exports = function(auth, app, passport) {
    
    app.post('/login', passport.authenticate('local'), function(req, res) {
      res.status(200).json({user: req.user});
    });
    
    app.get('/user/auth2', auth.isAuthenticated, function(req, res) {
        console.log("/user/auth2 .....");
        res.status(200).json({ user: "test" });
    });

    // TODO check the isAuthenticated
    app.get('/user/auth', auth.isAuthenticated, function(req, res) {
      res.status(200).json({ user: req.user });
    });

  /*  app.get("/oauth/google", passport.authenticate("google", {
        scope: ["email", "profile"]
    }));

    app.get("/oauth/google/callback", passport.authenticate("google", {
        successRedirect: HOME_PAGE,
        failureRedirect: SIGNIN_PAGE
    }));

    app.get("/oauth/facebook", passport.authenticate("facebook", {
        scope: ["email", "public_profile"]
    }));

    app.get("/oauth/facebook/callback", passport.authenticate("facebook", {
        successRedirect: HOME_PAGE,
        failureRedirect: SIGNIN_PAGE,
        failureFlash: true
    }));*/
  
    app.get('/logout', function(req, res) {
      req.logout();
      res.redirect('/');
    });
  };