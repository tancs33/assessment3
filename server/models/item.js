
module.exports = function (connection, Sequelize) {
    var Item = connection.define('items', {

        itemID: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        
            productName: {
                type: Sequelize.STRING,
                allowNull: false
            },


            productBrand: {
                type: Sequelize.STRING,
                allowNull: false
            },

            sessionName: {
                type: Sequelize.STRING,
                allowNull: false
            }
        },
        {
            // don't add timestamps attributes updatedAt and createdAt
            timestamps: false ,
            tableName: "item"
         });

    return Item;
};