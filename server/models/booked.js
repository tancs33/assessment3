module.exports = function (connection, Sequelize) {
    var Book = connection.define('book', {
            bookingID: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                references: {
                    model: 'booked',
                    key: 'bookingID'
                }
            },

            memID: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                references: {
                    model: 'member',
                    key: 'memID'
                }
            },


         
            dateBooked: {
                type: Sequelize.DATE,
                allowNull: false
            },
            timeBooked: {
                type: Sequelize.CHAR(45),
                allowNull: false
            }
        },
        {
            // don't add timestamps attributes updatedAt and createdAt
            timestamps: false,
            tableName: "booked"
        }
    );

    return Book;
};