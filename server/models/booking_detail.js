module.exports = function (connection, Sequelize) {
    var BookingDetail = connection.define('bookingdetails', {
            bookingDetailID: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                references: {
                    model: 'booking_detail',
                    key: 'bookingDetailID'
                }
            },

            itemID: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                    references: {
                    model: 'item',
                    key: 'itemID'
                }
            },
         /*  dept_no: {
                type: Sequelize.CHAR(4),
                allowNull: false,
                primaryKey: true,
                references: {
                    model: 'departments',
                    key: 'dept_no'
                }
            },*/
            qty: {
                type: Sequelize.INTEGER(11),
                allowNull: false
            },
            timeBooked: {
                type: Sequelize.STRING,
                allowNull: false
            }
        },
        {
            // don't add timestamps attributes updatedAt and createdAt
            timestamps: false,
            tableName: "booking_detail"
        }
    );

    return BookingDetail;
};