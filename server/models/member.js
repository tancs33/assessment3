module.exports = function(conn, Sequelize){
      var Member = conn.define('members', {
        memID: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false
        },
        first_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        last_name:{
            type: Sequelize.STRING,
            allowNull: false
        },
        gender:{
            type: Sequelize.ENUM('M', 'F'),
            allowNull: false
        },

        date_join: {
            type: Sequelize.DATE,
            allowNull: false
        },
        dateBooked: {
            type: Sequelize.DATE,
            allowNull: false
        },
      /*  photourl: {
            type: Sequelize.STRING,
            defaultValue: "https://upload.wikimedia.org/wikipedia/commons/d/de/Facebook_head.png"
        }*/
    }, {
        timestamps: false,
        tableName: "member"
    });
    return Member;
}